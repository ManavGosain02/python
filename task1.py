basePrice = int(input("Enter the price of the dress: "))
discount = int(input("Enter the discount percentage: "))
discount = discount + 10 if basePrice > 1999 else discount
print("Total discount: ", discount)
tax = 0
tax = 12 if basePrice > 1999 else tax
print("The tax due is: ", tax)
finalPrice = (basePrice * (tax/100)) - (basePrice * (discount/100)) + basePrice
print("The final price to be paid is: ", round(finalPrice))
